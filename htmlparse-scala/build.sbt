name := "htmlparse-scala"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "org.jsoup" % "jsoup" % "1.8.1"

libraryDependencies += "net.sf.jxls" % "jxls-reader" % "1.0.6"

libraryDependencies += "net.sf.jxls" % "jxls-core" % "1.0.6"

