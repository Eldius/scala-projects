import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements

object worksheet {
  var url = "http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso=" + 1661

  def getLoteryResults(url: String): List[String] = {

    def getDocument(url: String): Document = {
      Jsoup.connect(url).get()
    }

    def getResults(document: Document): Elements = {
      val tables = document.select(".num_sorteio")
      tables.get(0).select("li")
    }

    def parseResults(elements: Elements): List[String] = {
      var numbers = List[String]()
      elements.toArray().foreach(elem => numbers = elem.toString() :: numbers)
      numbers
    }

    val doc = getDocument(url)
    var resultElements = getResults(doc)

    parseResults(resultElements)
  }

  println(getLoteryResults(url))
}
