package net.eldiosantos.scala

import net.eldiosantos.scala.console.AppConsole
import net.eldiosantos.scala.lottery.generator.NumbersGenerator
import net.eldiosantos.scala.lottery.getter.GetLotteryNumber

/**
 * Created by eldio.junior on 16/12/2014.
 */
object MainApp extends App{
  def getResult() {
    var url = "http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso=" + 1661

    println(new GetLotteryNumber().getLoteryResults(url))
  }

  def generateNumbers(): Unit = {
    println(new NumbersGenerator().generate(1,60,6))
  }

  def testMenu(): Unit = {
    new AppConsole().execute()
  }

  testMenu()
}

