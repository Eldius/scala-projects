package net.eldiosantos.scala.lottery.getter;

import org.jsoup.Jsoup
import org.jsoup.nodes.{Element, Document}
import org.jsoup.select.Elements

/**
 * Created by eldio.junior on 30/12/2014.
 */
class GetLotteryNumber {
  def getLoteryResults(url: String): List[String] = {

    def getDocument(url: String): Document = {
      Jsoup.connect(url).get()
    }

    def getResults(document: Document): Elements = {
      val tables = document.select(".num_sorteio")
      tables.get(0).select("li")
    }

    def parseResults(elements: Elements): List[String] = {
      var numbers = List[String]()
      elements.toArray().foreach(elem => numbers = elem.asInstanceOf[Element].html() :: numbers)
      numbers
    }

    val doc = getDocument(url)
    var resultElements = getResults(doc)

    parseResults(resultElements)
  }
}


