package net.eldiosantos.scala.console

import net.eldiosantos.scala.lottery.generator.NumbersGenerator
import net.eldiosantos.scala.lottery.getter.GetLotteryNumber

import scala.io.StdIn

/**
 * Created by eldio.junior on 30/12/2014.
 */
class AppConsole {

  def execute(): Unit = {
    def generateNumbers(): String = {
      new NumbersGenerator().generate(1,60,6).mkString(", ")
    }

    def getResult(): String = {
      val url = "http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso="

      println("Please enter the result number:")
      val number = StdIn.readInt()

      new GetLotteryNumber().getLoteryResults(url + number).mkString(", ")
    }

    def exit() = "0"

    val options = List(
      new MenuOption(
        0
        , "Exit"
        , exit
      )
      , new MenuOption(
        1
        , "Generate numbers"
        , generateNumbers
      )
      , new MenuOption(
        2
        , "Verify results"
        , getResult
      )
    )
    var result = ""
    while(result != "0") {
      result = new MenuMaker().showMenu(options).getAction()()
      println(result)
    }
  }
}
