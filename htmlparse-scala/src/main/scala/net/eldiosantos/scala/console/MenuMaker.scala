package net.eldiosantos.scala.console

import scala.io.StdIn

/**
 * Created by eldio.junior on 30/12/2014.
 */
class MenuMaker {

  def showMenu(): Int = {
    val options = Map[Int, String](
      0 -> "Exit"
      , 1 -> "Generate numbers"
      , 2 -> "Verify results"
    )
    println("Choose an option:")
    options.foreach((e) => println(s"${e._1}) ${e._2}"))
    StdIn.readInt()
  }

  def showMenu(options: List[MenuOption]): MenuOption = {
    def error() = "Error"

    println("Choose an option:")
    options.foreach((e: MenuOption) => println(s"${e.getOption}) ${e.getDescription}"))
    val opt = StdIn.readInt()

    val choose = options.filter(e => e.getOption == opt)

    if(!choose.isEmpty) choose(0)
    else new MenuOption(0, "Error", error)
  }
}
