package net.eldiosantos.scala.console.menu

/**
 * Created by eldio.junior on 30/12/2014.
 */
class MenuOption(option: Int, description: String, action: () => String = () => ("no method")) {

  def getOption() = option
  def getDescription() = description
  def getAction() = action
}
