package net.eldiosantos.scala.console.menu

import scala.io.StdIn

/**
 * Created by eldio.junior on 30/12/2014.
 */
class MenuMaker(options: List[MenuOption]) {
  def showMenu(): MenuOption = {
    def error() = "Error"

    println("Choose an option:")
    options.foreach((e: MenuOption) => println(s"${e.getOption}) ${e.getDescription}"))
    val opt = StdIn.readInt()

    val choose = options.filter(e => e.getOption == opt)

    if(!choose.isEmpty) choose(0)
    else new MenuOption(0, "Error", error)
  }
}
