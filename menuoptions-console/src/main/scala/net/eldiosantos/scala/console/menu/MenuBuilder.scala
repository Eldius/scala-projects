package net.eldiosantos.scala.console.menu

import scala.collection.mutable.ListBuffer

/**
 * Created by eldio.junior on 03/03/2015.
 */
class MenuBuilder {

  val options = ListBuffer.empty[MenuOption]

  def addOption(opt: MenuOption): MenuBuilder = {
    options += opt
    this
  }

  def build() = {
    new MenuMaker(options.toList).showMenu()
  }
}
