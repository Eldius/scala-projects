package net.eldiosantos.scala.lottery.generator

import scala.util.Random

/**
 * Created by eldio.junior on 30/12/2014.
 */
class NumbersGenerator {

  def generate(rangeStart: Int, rangeFinish: Int, qtdNumbers: Int): List[Int] = {

    def createList(qtd: Int, numbers: List[Int]): List[Int] = {
      def randomNumber(): Int = new Random().nextInt(rangeFinish - rangeStart) + rangeStart

      if(qtd == 1) List[Int](randomNumber())
      else randomNumber() :: createList(qtd - 1, numbers)
    }

    createList(qtdNumbers, List[Int]())
  }
}
