call cls

setlocal enableDelayedExpansion 

set ROOT_DIR=%CD%
for /F %%x in ('dir /B/D %ROOT_DIR%') do (
  cd "%ROOT_DIR%\%%x\"
  echo ===========================  Configuring %CD% ===========================
  call sbt package
  call sbt clean
  call sbt eclipse
  cd "%ROOT_DIR%"
)

