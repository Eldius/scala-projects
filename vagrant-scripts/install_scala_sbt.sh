#!/bin/sh
 
# one way (older scala version will be installed)
# sudo apt-get install scala
cd /tmp

#2nd way
sudo apt-get remove scala-library scala
wget http://downloads.typesafe.com/scala/2.11.0/scala-2.11.0.deb
sudo dpkg -i scala-2.11.0.deb
sudo apt-get update
sudo apt-get install scala

# sbt installation
# remove sbt:>  sudo apt-get purge sbt. 

wget http://dl.bintray.com/sbt/debian/sbt-0.13.2.deb
sudo dpkg -i sbt-0.13.2.deb
sudo apt-get update
sudo apt-get install sbt

