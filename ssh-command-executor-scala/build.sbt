name := "ssh-command-executor-scala"

version := "1.0"

scalaVersion := "2.11.4"

libraryDependencies += "com.decodified" % "scala-ssh_2.11" % "0.7.0"

libraryDependencies += "org.slf4j" % "slf4j-log4j12" % "1.7.9"

libraryDependencies += "com.jcraft" % "jsch" % "0.1.51"

libraryDependencies ++= Seq(
  "org.bouncycastle" % "bcprov-jdk16" % "1.46",
  "com.jcraft" % "jzlib" % "1.1.3"
)

