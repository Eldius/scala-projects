package net.eldiosantos.scala.process.executor

import com.jcraft.jsch._

/**
 * Created by Eldius on 04/01/2015.
 */
class SftpExecutor (host1: String, port1: Int, user1: String, pass1: String, jSch1: JSch = new JSch) extends Executor {

    val user = user1
    val pass = pass1
    val host = host1
    val port = port1
    val jSch = jSch1

    def execute(command: String): String = {
      def action(channel: Channel, session: Session): String = {
        val ch = channel.asInstanceOf[ChannelSftp]
        ch.connect(3000)
        val params = command.split(";")
        val orig: String = params(0)
        val dest: String = params(1)

        if(orig.startsWith("local:")) {
          ch.put(orig.replace("local:", ""), dest.replace("remote:", ""))
        } else if(orig.startsWith("remote:")) {
          ch.get(orig.replace("remote:", ""), dest.replace("local:", ""))
        } else {
          return "Wrong usage"
        }

        "Finished"
      }

      super.exec("sftp", action)
    }
  }
