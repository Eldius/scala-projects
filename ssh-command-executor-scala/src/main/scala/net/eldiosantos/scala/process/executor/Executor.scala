package net.eldiosantos.scala.process.executor

import com.jcraft.jsch.{ChannelSftp, Channel, JSch, Session}

/**
 * Created by Eldius on 04/01/2015.
 */
trait Executor {

  val user: String
  val pass: String
  val host: String
  val port: Int
  val jSch: JSch

  def getSession(): Session = {
    val session = jSch.getSession(user, host, port)
    session.setPassword(pass)
    session.setConfig("StrictHostKeyChecking", "no");

    session
  }

  def execute(command: String): String

  protected def getChannel(session: Session, channelType: String): Channel = {
    session.openChannel(channelType)
  }

  protected def exec(channelType: String, action: (Channel, Session) => String): String = {
    val session = getSession()
    session.connect()

    val channel = getChannel(session, channelType)

    val response = action(channel, session)

    if(channel.isConnected) {
      channel.disconnect()
    }
    if(session.isConnected) {
      session.disconnect()
    }

    response
  }
}
