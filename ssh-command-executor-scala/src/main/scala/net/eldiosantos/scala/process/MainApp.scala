package net.eldiosantos.scala.process

import java.io.{ByteArrayOutputStream, OutputStream}

import com.jcraft.jsch._
import net.eldiosantos.scala.process.executor.{SftpExecutor, SshExecutor}

/**
 * Created by Eldius on 04/01/2015.
 */
object MainApp extends scala.App{

  val host = "localhost"
  val port = 2222
  val user = "vagrant"
  val pass = "vagrant"

  sendFile
  executeCommand

  def executeCommand: Any = {
    val executor = new SshExecutor(host, port, user, pass)

    println(executor.execute(" uname -a >> /vagrant/target/log.txt && cat /vagrant/target/log.txt"))
  }

  def sendFile: Any = {
    val executor = new SftpExecutor(host, port, user, pass)
    executor.execute("local:src/main/resources/log4j.xml;remote:/vagrant/target")
    executor.execute("remote:/home/vagrant/postinstall.sh;local:target")
  }
}
