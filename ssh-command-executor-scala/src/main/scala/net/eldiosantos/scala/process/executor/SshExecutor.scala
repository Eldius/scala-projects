package net.eldiosantos.scala.process.executor

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, OutputStream}

import com.jcraft.jsch.{Session, Channel, ChannelExec, JSch}

/**
 * Created by Eldius on 04/01/2015.
 */
class SshExecutor (host1: String, port1: Int, user1: String, pass1: String, jSch1: JSch = new JSch) extends Executor {

  val user = user1
  val pass = pass1
  val host = host1
  val port = port1
  val jSch = jSch1

  def execute(command: String): String = {
    def action(channel: Channel, session: Session): String = {
      val ch = channel.asInstanceOf[ChannelExec]
      ch.setCommand(command)
      ch.connect()

      val in = ch.getInputStream
      val response = scala.io.Source.fromInputStream(in).getLines().mkString("\n")

      response
    }

    super.exec("exec", action)
  }
}
