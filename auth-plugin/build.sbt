name := "auth-plugin"

version := "0.1"

scalaVersion := "2.10.3"

libraryDependencies ++= Seq(
  "junit"                        % "junit"              % "4.10"           % "test",
  "com.novocode"                 % "junit-interface"    % "0.10"           % "test",
  "com.github.kristofa"          % "mock-http-server"   % "1.3"            % "test",
  "org.scalaj"                   % "scalaj-http_2.10"   % "0.3.14",
  "org.apache.derby"             % "derby"              % "10.4.1.3"       % "test",
  "com.thoughtworks.xstream"     % "xstream"            % "1.4.7"
)


