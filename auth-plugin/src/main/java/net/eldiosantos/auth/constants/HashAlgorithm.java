package net.eldiosantos.auth.constants;

/**
 * Created by esjunior on 12/05/2014.
 */
public enum HashAlgorithm {

    AES("AES")
    , Blowfish("Blowfish")
    , DES("DES")
    , DESede("DESede")
    , DiffieHellman("DiffieHellman")
    , DSA("DSA")
    , OAEP("OAEP")
    , PBE("PBE")
    , RC2 ("RC2")
    , MD5 ("MD5");

    private final String algorithm;

    HashAlgorithm(String algorithm){
        this.algorithm = algorithm;
    }
}
