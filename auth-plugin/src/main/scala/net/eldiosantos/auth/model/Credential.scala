package net.eldiosantos.auth.model

/**
 * Created by esjunior on 12/05/2014.
 */
class Credential(userName: String, password: String, isRaw: Boolean) {

  def user: String = userName

  def pass: String = password

  def raw: Boolean = isRaw
}
