package net.eldiosantos.auth.model

class User extends Serializable {
  var credential: Credential = null

  def setCredential(credential: Credential): Unit = {
    if(!credential.raw)
      this.credential = credential
    else
      throw new IllegalArgumentException("Invalid credential raw state.")
  }

}