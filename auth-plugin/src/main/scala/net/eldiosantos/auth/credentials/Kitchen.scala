package net.eldiosantos.auth.credentials

import java.security.MessageDigest
import com.thoughtworks.xstream.XStream
import net.eldiosantos.auth.model.Credential

class Kitchen (digest1: MessageDigest){

  def digest = digest1
  
  def cook(credential: Credential): Credential = {
    new Credential(
		credential.user
		, digest.digest(credential.pass.getBytes).map("%02x".format(_)).mkString
		, false
    )
  }
  
  def serialize(credential: Credential): String = {
    new XStream().toXML(credential)
  }
}
