package net.eldiosantos.auth.credentials

import net.eldiosantos.auth.model.Credential

class CredentialValidator {

  def validate(credential: Credential): Boolean = {
    if(credential.raw){
      false
    }else{
      true
    }
  }
}