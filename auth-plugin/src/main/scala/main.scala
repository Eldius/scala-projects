import java.security.MessageDigest
import net.eldiosantos.auth.constants.HashAlgorithm
import net.eldiosantos.auth.model.Credential
import net.eldiosantos.auth.credentials.Kitchen
import net.eldiosantos.auth.credentials.Kitchen
import com.thoughtworks.xstream.XStream

object Main extends App {

  val digest = MessageDigest.getInstance("SHA-512")

  def raw = new Credential("user", "pass", true)
  
  println(raw.raw)
  
  def kitchen = new Kitchen(digest)
  
  println(kitchen.serialize(raw))
  
  def credential = kitchen.cook(raw)
  
  println(kitchen.serialize(credential))
}
