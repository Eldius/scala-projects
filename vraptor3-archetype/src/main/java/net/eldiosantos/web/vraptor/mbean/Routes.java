package net.eldiosantos.web.vraptor.mbean;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import br.com.caelum.vraptor.http.route.Route;
import br.com.caelum.vraptor.http.route.Router;
import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

@Component
@ApplicationScoped
public class Routes implements RoutesMBean {

	private final List<String> routes;

	public Routes(Router router){
		final List<Route> allRoutes = router.allRoutes();
		routes = new Vector<>();
		final Iterator<Route>it = allRoutes.iterator();
		
		Route r;
		while(it.hasNext()){
			r = it.next();
			routes.add(r.getOriginalUri());
		}

	}
	
	public List<String> getRoutes() {
		return this.routes;
	}
}