package net.eldiosantos.web.vraptor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;

/**
 * Created by esjunior on 26/03/14.
 */

@Resource
public class IndexController {

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Path("/")
    public void index(){
    	logger.debug("loading index page...");
    }
}
