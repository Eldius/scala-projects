package net.eldiosantos.web.vraptor.repository;

import java.io.Serializable;
import java.util.List;

public interface Read<T, K extends Serializable>
{
    public T getByPk(K pk);
    public List<T>listAll();
    public List<T>list(int offset, int size);
    public List<T>listByExemple(T ex);
}
