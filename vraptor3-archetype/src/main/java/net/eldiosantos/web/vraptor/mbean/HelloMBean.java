package net.eldiosantos.web.vraptor.mbean;

/**
 * Created by esjunior on 22/05/2014.
 */
public interface HelloMBean {
    public void sayHello();
    public int add(int x, int y);

    public String getName();

    public int getCacheSize();
    public void setCacheSize(int size);
}
