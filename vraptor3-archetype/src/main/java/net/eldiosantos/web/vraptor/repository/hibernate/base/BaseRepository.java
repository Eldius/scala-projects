package net.eldiosantos.web.vraptor.repository.hibernate.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import net.eldiosantos.web.vraptor.repository.Repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Example;


public class BaseRepository<T, K extends Serializable> implements Repository<T, K>
{
    protected Session session;
    protected Class<?>clazz;
    
    public BaseRepository(Session session){
      this.session = session;
      this.clazz = (Class<?>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }


    @Override
    public void delete(T element) {
        session.delete(element);
    }

    @Override
    public T getByPk(K pk) {
        return (T) session.get(clazz, pk);
    }

    @Override
    public List<T> listAll() {
        return session.createCriteria(clazz).list();
    }

    @Override
    public List<T> list(int offset, int size) {
        return session.createCriteria(clazz).setFirstResult(offset).setFetchSize(size).list();
    }

    @Override
    public List<T> listByExemple(T ex) {

        Example example = Example.create(ex)
                .enableLike()
                .excludeZeroes()
                .ignoreCase();

        Criteria criteria = this.session.createCriteria(this.clazz)
                .add(example);

        return criteria.list();
    }

    @Override
    public K save(T element) {
        return (K) session.save(element);
    }

    @Override
    public void update(T element) {
        session.update(element);
    }

    @Override
    public void saveOrUpdate(T element) {
        session.saveOrUpdate(element);
    }
}
