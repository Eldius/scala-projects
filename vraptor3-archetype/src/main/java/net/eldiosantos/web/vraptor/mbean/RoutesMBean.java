package net.eldiosantos.web.vraptor.mbean;

import java.util.List;

public interface RoutesMBean {

	List<String>getRoutes();
}
