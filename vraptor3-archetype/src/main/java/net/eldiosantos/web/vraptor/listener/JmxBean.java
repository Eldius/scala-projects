package net.eldiosantos.web.vraptor.listener;

import java.lang.management.ManagementFactory;

import javax.annotation.PostConstruct;
import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.eldiosantos.web.vraptor.mbean.Hello;
import net.eldiosantos.web.vraptor.mbean.Routes;
import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.caelum.vraptor.ioc.Component;

/**
 * Created by esjunior on 22/05/2014.
 */

@Component
@ApplicationScoped
public class JmxBean {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Hello hello;
	private Routes routes;

    public JmxBean(Hello hello, Routes routes) {
		super();
		this.hello = hello;
		this.routes = routes;
	}

    @PostConstruct
	public void setup() throws MalformedObjectNameException, NotCompliantMBeanException, InstanceAlreadyExistsException, MBeanRegistrationException, InterruptedException {

    	logger.info("Iniciando MBeans...");

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("net.eldiosantos.test.jmx:type=Hello");
        mbs.registerMBean(hello, name);

        name = new ObjectName("net.eldiosantos.test.jmx:type=Routes");
        mbs.registerMBean(routes, name);
        
        logger.info("MBeans registrados: " + mbs.getMBeanCount());

        logger.info("MBeans iniciados...");
    }

}
