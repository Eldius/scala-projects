package net.eldiosantos.web.vraptor.repository;

import java.io.Serializable;

public interface Delete<T, K extends Serializable>
{
    public void delete(T element);
}
