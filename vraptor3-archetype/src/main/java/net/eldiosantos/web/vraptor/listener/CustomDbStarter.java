package net.eldiosantos.web.vraptor.listener;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.h2.server.web.DbStarter;
import org.h2.tools.Server;
import org.h2.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Customization of {@link DbStarter} class that only starts
 * if logger is in debug mode.
 * @author Eldius
 *
 */
@WebListener
public class CustomDbStarter implements ServletContextListener {

	private Logger logger = LoggerFactory.getLogger(getClass());

    private Connection conn;
    private Server server;

	
	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		if(logger.isDebugEnabled()){
			this.startH2DbServer(servletContextEvent);
			this.showDatabaseInfo();
		}
	}

	private void showDatabaseInfo(){
		logger.info("server stated at " + server.getURL());
		File dbFile = new File("~/target/database.db");
		logger.info("database file: " + dbFile.getAbsolutePath());
	}
	
    public void startH2DbServer(ServletContextEvent servletContextEvent) {
        try {
            org.h2.Driver.load();

            // This will get the setting from a context-param in web.xml if defined:
            ServletContext servletContext = servletContextEvent.getServletContext();
            String url = getParameter(servletContext, "db.url", "jdbc:h2:~/target/database.db");
            String user = getParameter(servletContext, "db.user", "sa");
            String password = getParameter(servletContext, "db.password", "sa");

            // Start the server if configured to do so
            String serverParams = getParameter(servletContext, "db.tcpServer", null);
            if (serverParams != null) {
                String[] params = StringUtils.arraySplit(serverParams, ' ', true);
                server = Server.createTcpServer(params);
                server.start();
            }

            // To access the database in server mode, use the database URL:
            // jdbc:h2:tcp://localhost/~/test
            conn = DriverManager.getConnection(url, user, password);
            servletContext.setAttribute("connection", conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getParameter(ServletContext servletContext, String key, String defaultValue) {
        String value = servletContext.getInitParameter(key);
        return value == null ? defaultValue : value;
    }

    /**
     * Get the connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        return conn;
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        try {
            Statement stat = conn.createStatement();
            stat.execute("SHUTDOWN");
            stat.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (server != null) {
            server.stop();
            server = null;
        }
    }

}
