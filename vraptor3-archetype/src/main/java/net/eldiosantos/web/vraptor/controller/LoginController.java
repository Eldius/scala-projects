package net.eldiosantos.web.vraptor.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;

@Resource
@Path("login")
public class LoginController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@Path("/")
	public void index(){
		logger.debug("loading index of login...");
	}
}
