package net.eldiosantos.web.vraptor.interceptor;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.ioc.RequestScoped;
import br.com.caelum.vraptor.resource.ResourceMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Eldius on 21/02/14.
 */

@Intercepts
@RequestScoped
public class UrlInterceptor implements Interceptor{

    private Logger logger = LoggerFactory.getLogger(getClass());

    private Result result;
    private HttpServletRequest request;

    public UrlInterceptor(Result result, HttpServletRequest request) {
        this.result = result;
        this.request = request;
    }

    @Override
    public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) throws InterceptionException {
        logger.debug("pathinfo " + request.getPathInfo());
        logger.debug("request.getRequestURL() " + request.getRequestURL());
        logger.debug("request.getServletPath() " + request.getServletPath());
        result.include("url", request.getServletPath());
        String[] split = request.getServletPath().split("/");

        String menu = null;

        if(split.length > 1){
            menu = split[1];
        }else{
            menu = "index";
        }

        result.include("menu", menu);
        stack.next(method, resourceInstance);
    }

    @Override
    public boolean accepts(ResourceMethod method) {
        return true;
    }
}
